



/*
//11.01,2024

const iterable = [0,1 ,2];
for (let i of iterable) {
    console.log(i);
}

for (let key of Object.keys(obj)) {
    console.log (key+":"+obj[key])
}

console.log("....................") 

for (let key in obj) {
    console.log (key + ":" + obj[key])
}

/////////////// WHILE LOOP ///////

var i = 0
while (i<1000) {
    console.log (i)
    i+=5
}

do (
    console.log(i)
    i+=5
) while(i<1000)



///// IF
var x = 78
if (x>6777) {
    console.log ("Sii es mayor")

} else {
    console.log ("NOOO no es mayor")
}


// OPERADOR TERNARIO 

var (armando === "Armandin") ? "SIII, este si es Armando" : "NOO este no es Armando"
console.log(armando)

// ANIDADO IF 
//Corta flujo despeus de ejecutar instrucciones, si no es cortado va por el siguiente caso
//Te pide un case al final para en caso que no exista la lista en el, para no marcar error, si sale error se corta flujo del programa 

var classroom = 1
switch (classroom) {
    case 1:
        console.log ("CLassroom 1")
        break; 
    default:
        console.log ("Classroom no encontrado")
        break;
}



//FUNCIONES SCOOPING 
//

var base = 10
var altura = 20

function area (base, altura) {
    return (base * altura )/2 
}

console.log("El area del triangulo es: "  + area (base, altura))



///FUNCIONES ANONIMAS, tienen caracteristicas de una funcion pero no tienen nombre 
//CAda funcion declara a la otra funcion y la manda a llamar 

var i= 1
var w= 27
var h= 67
function prism (i) {
    return function (w) {
        return function (h) {
            return 1 * w * h;
        }
    }
}
console.log ("EL volumen del prisma es: " + prism(i))


//FUNCIONES ANONIMAS, puede ser invocada despues que se termina la funcion anonima 

(function(){
    console.log("FUncion anonima")
}());

//FUNCIONES NOMBRADAS, se invocan por nombre

const fx = function sum(){
    return x + y 
}

const fy = function() {
    return g + h
}

console.log("fx: " + fx(23,321)+"fy: "+fy(31,34))



//

//Nombrada
var namedSum = function sum (a,b) {
    return a + b;
}

//Anonima
var anonSum = function (a,b) {
    return a + b;
}

namedSum (1,3);
anonSum (1,3);


//Recursividad 

var say = function (times) {
    if (times>0) {
        console.log ("HOlaaa");
        say(times-1); 
    }
}

var sayholaa = say 
sayholaa(10)



function logSOmeTHings() {
    for (var i = 0; i < arguments.length; ++i){
        console.log(arguments[i]);
    }
}

logSOmeTHings ("OLaaa", "Mundooo")


//Funcion arrow plana 

function personLogsSomeThings(person, ...msg){
    msg.forEach(arg => {
        console.log(person, "SAys", arg);
    });
}
personLogsSomeThings("Armando", "Tercero", "UwU");

const logarg = (...args) => console.log (args)
const list = [1,2,3,4]

logarg("a","b","c",...list)

//Ya que vienen los 3 puntitos hay un arreglo de parametros 



function personSay(person,...msg) {
    msg.forEach(arg=>{
        console.log(person+ "say:" +arg) 
    })
}
personSay("HOla ", "Hello", "BOnjour", "NI HAo", "JiaFei", "XD")



 ///NUEVO METODO DE CONEXION !! ///

const axios = require('axios')
var url = "https://jsonplaceholder.typicode.com/posts"
axios.get(url).then((result) => {
    console.log(result)
}).catch((err) => {
    console.log(error)
});



//V2
const axios = require('axios')
var url = "https://jsonplaceholder.typicode.com/posts"
axios.get(url).then(({data}) => {
    for (const key in data){
     console.log(data[key].title)
    }
}).catch((err) => {
    console.log(err)
});

*/

//V3
const axios = require('axios')
var url = "https://jsonplaceholder.typicode.com/posts"
axios.post(url,{
    userId: 2,
    title: "Hola el viernes es viernes"

}).then(({data}) => console.log(data))




